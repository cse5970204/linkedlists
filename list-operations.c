#include <stdio.h>
#include <stdlib.h>

typedef struct Node {
    int number;
    struct Node *next;
} theNode;

// Function prototypes
theNode* createNode(int num);
void printList(struct Node *head);
void append(struct Node **head, int num);
void prepend(struct Node **head, int num);
void deleteByKey(struct Node **head, int key);
void deleteByValue(struct Node **head, int value);
void insertAfterKey(struct Node **head, int key, int value);
void insertAfterValue(struct Node **head, int searchValue, int newValue);

int main() {
    theNode *head = NULL;
    int choice, data;

    while (1) {
        printf("Linked Lists\n");
        printf("1. Print List\n");
        printf("2. Append\n");
        printf("3. Prepend\n");
        printf("4. Delete\n");
        printf("5. Insert\n");
        printf("6. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch (choice) {
            case 1:
                if (head == NULL) {
                    printf("Head is null, nothing to show.\n");
                    break;
                }
                printList(head);
                break;
            case 2:
                printf("Enter data to append: ");
                scanf("%d", &data);
                append(&head, data);
                break;
            case 3:
                printf("Enter data to prepend: ");
                scanf("%d", &data);
                prepend(&head, data);
                break;
            case 4:
                printf("Select delete method:\n1. Delete by key (zero- indexed)\n2. Delete by value\nEnter method: ");
                int delMethod;
                scanf("%d", &delMethod);
                if (delMethod == 1) {
                    printf("Enter key: ");
                    scanf("%d", &data);
                    deleteByKey(&head, data);
                    break;
                } else if(delMethod == 2) {
                    printf("Enter value: \n");
                    scanf("%d", &data);
                    deleteByValue(&head, data);
                    break;
                } else {
                    printf("Invalid delete entry\n");
                    break;
                }
            case 5:
                printf("Select insert method:\n1. Insert after key (zero- indexed)\n2. Insert after value\nEnter method: ");
                    int insMethod;
                    int insValue;
                    scanf("%d", &insMethod);
                    if (insMethod == 1) {
                        printf("Enter key after which to insert: ");
                        scanf("%d", &data);
                        printf("Enter value to insert: ");
                        scanf("%d", &insValue);
                        insertAfterKey(&head, data, insValue);
                        break;
                    } else if(insMethod == 2) {
                        printf("Enter value after which to insert: ");
                        scanf("%d", &data);
                        printf("Enter value to insert: ");
                        scanf("%d", &insValue);
                        insertAfterValue(&head, data, insValue);
                        break;
                    } else {
                        printf("Invalid insert entry\n");
                        break;
                    }
            case 6:
                printf("'Exit' selected..\n");
                exit(0);
            default:
                printf("Invalid choice. Please try again.\n");
        }
    }
    return 0;
}

theNode* createNode(int num) {
    theNode* node = (theNode*)malloc(sizeof(theNode));
    node->number = num;
    node->next = NULL;
    return node;
}

void printList(struct Node *head) {
    printf("[");
    theNode* currHead = head;
    while (currHead->next != NULL) {
        printf("%d, ", currHead->number);
        currHead = currHead->next;
    }
    if (currHead != NULL) {
        printf("%d", currHead->number);
    }
    printf("]\n");
}
void append(struct Node **head, int num) {
    theNode* node = createNode(num);
    theNode* currHead = *head;
    if (*head ==NULL) {
        *head = node;
        return;
    }
    while (currHead->next != NULL) {
        currHead = currHead->next;
    }
    currHead->next = node;
    return;
}
void prepend(struct Node **head, int num) {
    theNode* node = createNode(num);
    if (*head ==NULL){
        *head = node;
        return;
    }
    node->next = *head;
    *head = node;

}
void deleteByKey(struct Node **head, int key) {
    if (*head ==NULL) {
        printf("There is nothing to delete.\n");
        return;
    }
    if (key < 0) {
        printf("Key is not valid!\n");
        return;
    } else if (key ==0) {
        *head = (*head)->next;
    } else {
        theNode* currHead = *head;
        for (int i=1;i<key;i++) {
            if (currHead->next != NULL) {
                currHead = currHead->next;
            } else{
                printf("Key entered is not in range.\n");
                return;
            }
        }
        currHead->next = currHead->next->next;
    }
    printf("Successfully deleted key %d.\n", key);
}
void deleteByValue(struct Node **head, int value) {
    theNode* currHead = *head;
    theNode* prevHead =NULL;
    if (*head ==NULL) {
        printf("There is nothing to delete.\n");
        return;
    }
    while (currHead->next !=NULL) {
        if (currHead->number != value) {
            prevHead = currHead;
            currHead = currHead->next;
        } else {
            if (prevHead!=NULL) {
                prevHead->next = currHead->next;
                free(currHead);
                printf("Successfully deleted value %d.\n", value);
                return;
            } else {
                *head = (*head)->next;
                printf("Successfully deleted value %d.\n", value);
                return;
            }            
        }
    }
    if (currHead->number == value) {
        prevHead->next = NULL;
        free(currHead);
        printf("Successfully deleted value %d.\n", value);
    } else {
        printf("Value not found in list.\n");
    }
}
void insertAfterKey(struct Node **head, int key, int value) {
    if (*head ==NULL) {
    printf("There is no key to begin with.\n");
    return;
    }
    if (key < 0) {
        printf("Key is not valid!\n");
        return;
    }
    theNode* node = createNode(value);
    theNode* currHead = *head;
    while (key>0) {
        if (currHead->next !=NULL) {
            currHead = currHead->next;
        } else {
            printf("Key out of range!\n");
            return;
        }
        --key;
    }
    node->next = currHead->next;
    currHead->next = node;
    printf("Inserted the %d.", value);
}
void insertAfterValue(struct Node **head, int searchValue, int newValue) {
    if (*head ==NULL) {
        printf("There is no value to begin with.\n");
        return;
    }
    theNode* node = createNode(newValue);
    theNode* currHead = *head;
    while (currHead->next !=NULL) {
        if (currHead->number != searchValue){
            currHead = currHead->next;
        } else {
            node->next = currHead->next;
            currHead->next = node;
            printf("Inserted %d after %d.", newValue, searchValue);
            return;
        }        
    }
    if (currHead->number == searchValue) {
        currHead->next = node;
    } else {
        printf("Value has not been found.\n");
    }
}

